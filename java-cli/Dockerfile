FROM ubuntu:22.04

#### INSTALL COMMON COMMAND LINE TOOLS

# set timezone so that the following installs do not ask for it
# https://askubuntu.com/questions/909277/avoiding-user-interaction-with-tzdata-when-installing-certbot-in-a-docker-contai/1098881#1098881
ENV TZ=Europe/London
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# to check available versions:  apt-cache madison ansible   or    apt-cache policy ansible
RUN apt-get update && \
    apt-get install -y git curl vim ssh wget unzip python3 python3-pip iputils-ping net-tools && \
    apt-get install -y sshpass dos2unix && \
    pip3 install pandas


## SUPPORT APPS WITH UIs (eg intellij)
#RUN apt-get update && \
#    apt-get install -y 'x11vnc=0.9.16-3' \
#                       'xvfb' \
#                       'firefox'
#
#
####  INSTALL JDK17
#     See https://jdk.java.net/17/

ARG JDK_VERSION=17
ARG JDK_DOWNLOAD_URL=https://download.java.net/java/GA/jdk17.0.1/2a2082e5a09d4267845be086888add4f/12/GPL/openjdk-17.0.1_linux-x64_bin.tar.gz
ARG JDK_DIR_NAME=jdk-17.0.1
ENV JAVA_HOME=/opt/jdk/current

RUN curl -L -o /root/jdk.tgz $JDK_DOWNLOAD_URL && \
    mkdir -p /opt/jdk && \
    cd /opt/jdk && \
    tar -xf /root/jdk.tgz && \
    rm /root/jdk.tgz && \
    ln -s /opt/jdk/$JDK_DIR_NAME /opt/jdk/current


####  INSTALL TERRAFORM

ARG TERRAFORM_VER="1.0.10"
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VER}/terraform_${TERRAFORM_VER}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VER}_linux_amd64.zip && \
    mv terraform /usr/local/bin/ && \
    rm terraform_${TERRAFORM_VER}_linux_amd64.zip

#### INSTALL ANSIBLE

RUN apt-get install -y ansible=2.10.7+merged+base+2.10.8+dfsg-1


#### INSTALL GRADLE

# NB remember to update gradlew on each of the projects:  ./gradlew wrapper --gradle-version 7.3

ARG GRADLE_VERSION=7.3
ARG GRADLE_DOWNLOAD_URL=https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip
ARG GRADLE_DIR_NAME=gradle-${GRADLE_VERSION}

RUN curl -L -o /tmp/gradle.zip $GRADLE_DOWNLOAD_URL && \
    mkdir -p /opt/gradle && \
    cd /opt/gradle && \
    unzip /tmp/gradle.zip && \
    rm /tmp/gradle.zip && \
    ln -s /opt/gradle/$GRADLE_DIR_NAME /opt/gradle/current && \
    cd /tmp && \
    mkdir dummy-project && cd dummy-project && \
    /opt/gradle/current/bin/gradle init --type basic --dsl kotlin --incubating --project-name foo && \
    ./gradlew && \
    cd / && \
    rm -rf /tmp/dummy-project

##################

COPY files/ /

#ENTRYPOINT ["/bin/bash", "-l"]
CMD ["/bin/bash", "-l"]
