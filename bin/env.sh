#!/bin/bash

set -eu

IMAGE=${1:-java-ui}

# if PROJECT_DIR is not already set, then figure it out
if [ -z ${PROJECT_DIR+x} ]; then
    PROJECT_DIR="$(pwd)"
fi

if [ -z ${PROJECT_NAME+x} ]; then
    PROJECT_NAME="$(basename $PROJECT_DIR)"
fi


docker run -it --rm --privileged --name "env-${PROJECT_NAME}" --hostname "${PROJECT_NAME}" \
           --workdir /projects \
           -v "$PROJECT_DIR:/projects" -v "$HOME/.ssh:/root/.ssh" -v "$HOME/.gitconfig:/root/.gitconfig" \
           -v "$HOME/.docker:/root/.docker" -v "$HOME/.gradle-${PROJECT_NAME}:/root/.gradle" \
           -v "$HOME/.m2:/root/.m2" -v "$HOME/.config:/root/.config" \
           -e DISPLAY=host.docker.internal:0 \
           $IMAGE /bin/bash -l
